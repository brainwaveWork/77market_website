import colors from "vuetify/es5/util/colors";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "77market",
    title: "77market",
    htmlAttrs: {
      lang: "th",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/assets/css/main.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
    "@nuxtjs/dotenv",
    "@nuxtjs/axios",
    "cookie-universal-nuxt",
    [
      "@nuxtjs/google-analytics",
      {
        id: ["UA-215310359-1"],
      },
    ],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxt/http",
    [
      "@nuxtjs/google-analytics",
      {
        id: ["G-W5CV13CTCJ"],
      },
    ],
  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    treeShake: true,
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
        light: {
          "ci-orange": "#FF681C",
          "ci-orange-lt1": "#FB8C22",
          "ci-red": "#FF5252",
          "ci-brown": "#2E0F01",
          "ci-brown-dk1": "#2E2C2B",
          "ci-green": "#4CAF50",
          "ci-purple": "#9C27B0",
          "ci-blue": "#2251A3",
          "ci-grey": "#797979",
          "ci-grey-lt1": "#C3C3C3",
          "ci-grey-lt2": "#D5D5D5",
          "ci-grey-lt3": "#F8F8F8",
          "ci-grey-dk1": "#656565",
          "ci-grey-dk2": "#505050",
          "ci-grey-dk3": "#2E2C2B",
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  axios: {
    baseURL: `${process.env.SERVER_URL}`,
  },
  server: {
    port: `${process.env.SERVER_PORT}`,
    host: "0.0.0.0",
  },
};
